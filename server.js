var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var expressSession = require('express-session')({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
  });

var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var passport = require('passport');
var passportLocalMongoose = require('passport-local-mongoose');
var connectEnsureLogin = require('connect-ensure-login');

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressSession);
app.use(passport.initialize());
app.use(passport.session());

// Schemas & Models =========>

var UserDetail = new mongoose.Schema({
  username: String,
  password: String
});

UserDetail.plugin(passportLocalMongoose);
var UserDetails = mongoose.model('userInfo', UserDetail, 'userInfo');
var Message = new mongoose.model('Message', { 
    name : String, 
    message : String,
    created_at : { type: Date, required: true, default: Date.now }
});

var dbURL = "mongodb+srv://Param:P12345678@cluster0-rjd24.mongodb.net/chat-app?retryWrites=true&w=majority";

passport.use(UserDetails.createStrategy());
passport.serializeUser(UserDetails.serializeUser());
passport.deserializeUser(UserDetails.deserializeUser());

// Global Chat Routes =========>

app.get('/messages', (req, res) => {
    Message.find({},(err, messages)=> {
      res.send(messages);
    })
})

app.get('/latestMessages', (req, res) => {
    var showLatestMsgsLimit = 100
    Message.find({}, {}, { 
        sort: {'created_at': 'desc'},
        limit: showLatestMsgsLimit
    }, (err, messages)=> {
        if (err) {
            console.log(err);
          } else {
            console.log("\nLatest " + showLatestMsgsLimit + " Messages: " + messages);
            res.send(messages);
          }
    })
})

app.post('/messages', async (req, res) => {
    var maxGlobalChatCount = 100
    try {
        var globalMsgCount = await Message.find().countDocuments();
        console.log("\nGlobal Message Count: " + globalMsgCount);
        var message = new Message(req.body);

        var timeDifference = Date.now();
        var msgsByHost = await Message.find({name: message.name}, {}, {sort: {'created_at': 'desc'}})
        if (msgsByHost[1]) {
            timeDifference -= msgsByHost[1].created_at;
        }
        
        if (timeDifference > 10000 && message.name && message.message) {
            var savedMessage = await message.save();
            console.log('\nSaved in DB: ' + message);
            io.emit('message', req.body);
            res.sendStatus(200);

            if (globalMsgCount >= maxGlobalChatCount) {
                console.log('\nMessage Count Exceed. Deleting Oldest Message.');
                Message.findOneAndDelete({}, {sort: { 'created_at': 1 }}, function(err, post) {
                    console.log('\nDeleted from DB: ' + post);
                });
            }
        } else {
            res.sendStatus(501);
        }
    } catch (error) {
        res.sendStatus(500);
        return console.log('Error: ', error);
    } finally{
        console.log('\nMessage Posted on Chat');
    }
})

// Authentication Routes =========>

app.post('/login', (req, res, next) => {
    passport.authenticate('local',
    (err, user, info) => {
      if (err) {
        return next(err);
      }
  
      if (!user) {
        return res.redirect('/login?info=' + info);
      }
  
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
  
        return res.redirect('/');
      });
  
    })(req, res, next);
  });
  
  app.get('/login',
    (req, res) => res.sendFile('html/login.html', { root: __dirname })
  );
  
  app.get('/',
    connectEnsureLogin.ensureLoggedIn(),
    (req, res) => res.sendFile('html/index.html', {root: __dirname})
  );
  
  app.get('/user',
    connectEnsureLogin.ensureLoggedIn(),
    (req, res) => res.send({user: req.user})
  );

  app.get('/logout', function(req, res){
    req.logOut();
    return res.redirect('/');
  });

// Manually registering few users with Password same as Username  =========>
// For adding a New User to Database, just add Username to Users Array
Users = ['Param', 'Ananya', 'Omkar', 'Aadesh', 'Firoz', 'Sumit', 'Yash', 'Pushkar'];
Users.forEach(addUserIfNotExist);

async function addUserIfNotExist(user) {
    var uname = await UserDetails.findOne({username: user});
    if (!uname) {
      await UserDetails.register({username: user, active: false}, user);
    }
}

// Connections =========>

mongoose.connect(dbURL, { useNewUrlParser: true, useUnifiedTopology: true, })
    .then(() => console.log('\nDB Connected!'))
    .catch((error) =>
        JSON.stringify(error)
    );

io.on('connection', () => {
    console.log('\nA new user has joined the Global Chat');
})

var server = http.listen(3000, () => {
    console.log('\nServer is running on port', server.address().port);
});