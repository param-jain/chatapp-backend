costArray = [2500, 4000, 3500, 4000, 6000, 3500, 2000, 4000, 2500]
referenceArray = []
numberOfRows = 3
numberOfColumns = 3
column = 0

for index, val in enumerate(costArray):
    row = len(referenceArray)/numberOfRows+1
    column = column + 1
    if column > numberOfColumns:
        column = 1
        
    index = {
        "row": row,
        "column": column,
        "index": index,
        "original_value": val,
        "modified_value": val,
    }
    referenceArray.append(index)
    
newlist = sorted(referenceArray, key=lambda k: k['original_value'])
print 'List Sorted according to Original Cost:'
print newlist
print ''

columnsTested = []
columnAndValue = []

for i in newlist:
    testObj = {
        "column": i['column'],
        "minVal": i['original_value']
    }
    if i['column'] not in columnsTested:
        columnAndValue.append(testObj)
        columnsTested.append(i['column'])
        
for i in newlist:
    for j in columnAndValue:
        if i['column'] == j['column']:
            i['modified_value'] = i['modified_value'] - j['minVal']

newlist = sorted(newlist, key=lambda k: k['modified_value'])
print 'List Sorted after Column Modifications:'
print newlist
print ''

rowsTested = []
rowAndValue = []
for i in newlist:
    testObj = {
        "row": i["row"],
        "minVal": i["modified_value"],
        "modified": i["modified_value"]
    }
    if i['row'] not in rowsTested:
        rowAndValue.append(testObj)
        rowsTested.append(i['row'])

for i in newlist:
    for j in rowAndValue:
        if i['row'] == j['row'] and j['modified'] > 0:
            i['modified_value'] = i['modified_value'] - j['minVal']

newlist = sorted(newlist, key=lambda k: k['modified_value'])
print 'List Sorted after Row Modifications:'
print newlist
print ''
print ''

print 'FINAL ANSWER ====>'
print ''
finalRowsTested = []
for i in newlist:
    if i['modified_value'] == 0 and i['row'] not in finalRowsTested:
        finalRowsTested.append(i['row'])
        print ("Index = ", i['index']) 
        print ("From = ", i['row'])
        print ("To = ", i['column'])
        print ''
        
print ''
    